import React, {Component} from 'react';
import {combineReducers} from 'redux';

const sampleuser = {
	sid: "6629208B1B219A92F516DD6D7E4A144F",
	user: {
		name: "guan",
		email: "a@a.com"
	}
}

const user = (state = /*sampleuser*/{sid: null, user: null}, action) => {
	switch(action.type){
		case "RETURN_LOGIN":
			if (action.success){
				console.log(action.data);
				return ({
					sid: action.data.sid,
					user: {
						name: action.data.name,
						email: action.data.email
					}
				});
			}
			else{
				return ({
					sid: null,
					user: null
				});
			}
		case "LOGOUT":
			return (Object.assign({}, sampleuser, {user: null}));
		default:
			return state;
	}
}

const hasError = (state = false, action) => {
	switch (action.type){
		case "RETURN_LOGIN":
			if (!action.success){
				return "Invalid email / password";
			}
			else{
				return false;
			}
		default:
			return state;
	}
}

const isFetching = (state = false, action) => {
	switch (action.type){
		case "REQUEST_LOGIN":
			return true;
		case "RETURN_LOGIN":
			return false;
		case "REQUEST_GET_ITEMS":
			return true;
		case "RETURN_GET_ITEMS":
			return false;
		case "REQUEST_BUY":
			return true;
		case "RETURN_BUY":
			return false;
		default:
			return state;
	}
}

const shopItems = (state = [], action) => {
	switch (action.type){
		case "RETURN_GET_ITEMS":
			return action.data.data;
		default:
			return state;
	}
}

const selectedItem = (state = null, action) => {
	switch (action.type){
		case "SELECT_ITEM":
			return action.id;
		case "UNSELECT_ITEM":
			return null;
		case "REMOVE_SWAPPED_ITEM":
			return null;
		default:
			return state;
	}
}

const swappedItem = (state = null, action) => {
	switch (action.type){
		case "RETURN_BUY":
			return action.data;
		case "REMOVE_SWAPPED_ITEM":
			return null;
		default:
			return state;
	}
}


export const reducer = combineReducers({
	user,
	hasError,
	isFetching,
	shopItems,
	selectedItem,
	swappedItem
})