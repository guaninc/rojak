import 'babel-polyfill'
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {InfoTile} from 'adminlte-reactjs';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import {Home} from './pages/home/Home.js';
import {SmartStore} from "./pages/store/Store.js";
import Login from './pages/login/Login.js';
import {SmartBuy} from './pages/buy/Buy.js';
import {reducer} from './reducers/reducer.js';
import {Provider, connect} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import {logout} from './actions/action.js';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

const App = () => {

  return (<Provider store = {store}>
    <Router>
      <div>
        <SmartNavBar />

        <div className = "content-wrapper" style = {{minHeight: "850px"}}>
          <div className = "container">
            <div className = "content">
              <Route exact path = "/" component = {Home} />
              <Route path = "/login" component = {Login} />
              <Route path = "/store" component = {SmartStore} />
              <Route path = "/buy" component = {SmartBuy} />
            </div>
          </div>
        </div>
        <footer className = "main-footer">
          <p>Copyright © 2017 Rojak Pte Ltd. All Rights Reserved</p>
        </footer>
      </div>
    </Router>
  </Provider>);
}

const NavBar = ({doLogout, user }) => {
  /*return (
    <header className = "main-header">
      <div className = "container-fluid">
        <div className = "navbar-header">
          <Link to = "/"><span className = "kaushan">Rojak</span></Link>
          <button type = "button" className = "navbar-toggle collapsed" data-toggle = "collapse" data-target="#navbar-collapse">
            <i className = "fa fa-bars"></i>
          </button>
        </div>

        <div className = "collapse navbar-collapse" id = "navbar-collapse">
          <ul className = "nav navbar-nav">
            <li><Link to = "/">Shtuff</Link></li>
          </ul>
        </div>
      </div>
    </header>);*/

    const signin = <li><Link to = "/login">Sign In</Link></li>;
    let ans, welcome;
    let store = <div></div>;
    if (user){
      const signout = <li onClick = {() => doLogout()}><a href = "#">Sign Out</a></li>;
      welcome = <li><a href = "#">Welcome {user.name}</a></li>;
      ans = signout;
      store = <li><Link to = "/store">Store</Link></li>;
    }
    else{
      ans = signin;
    }
  return (
    <header className="main-header">
      <nav className="navbar navbar-static-top">
        <div className = "container">
          <div className = "navbar-header">
            <Link to = "/" className = "navbar-brand">
              <span className = "rojak-logo">Rojak</span>
            </Link>
          </div>

          <div className = "navbar-custom-menu pull-left">
            <ul className = "nav navbar-nav">
              <li><Link to = "/">Home</Link></li>
              {store}
            </ul>
          </div>
          <div className = "navbar-custom-menu">
            <ul className = "nav navbar-nav navbar-right">
              {welcome}
              {ans}
            </ul>
          </div>
        </div>
      </nav>
    </header>);
}

const mapStateToProps = (state) => {
  return {
    user: state.user.user
  };
}

const mapDispatchToProps = (dispatch) => {
  return ({
    doLogout: () => {dispatch(logout());}
  });
}

const SmartNavBar = connect(mapStateToProps, mapDispatchToProps)(NavBar);



export default App;
