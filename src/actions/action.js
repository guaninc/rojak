import fetch from 'isomorphic-fetch'

function requestLogin (email, password) {
	return {
		type: "REQUEST_LOGIN",
		email,
		password
	};
}

function returnLogin (data, success) {
	return {
		type: "RETURN_LOGIN",
		data,
		success
	};
}

function requestGetItems () {
	return {
		type: "REQUEST_GET_ITEMS"
	};
}

function returnGetItems(data, success){
	return {
		type: "RETURN_GET_ITEMS",
		data,
		success
	};
}

function requestBuy(){
	return {
		type: "REQUEST_BUY"
	};
}

function returnBuy(data, success){
	return {
		type: "RETURN_BUY",
		data,
		success
	}
}

export function selectItem(itemID){
	return {
		type: "SELECT_ITEM",
		id: itemID
	}
}

export function unselectItem(){
	return {
		type: "UNSELECT_ITEM"
	}
}

export function removeSwappedItem(){
	return {
		type: "REMOVE_SWAPPED_ITEM"
	}
}

export function checkLogin(email, password){
	return function(dispatch){
		dispatch(requestLogin(email, password));

		return fetch("http://localhost:4000/api/sessions", {
			method: "POST",
			body: JSON.stringify({
				sessions:{
					email,
					password
				}
				
			}),
			headers:{
				'Content-Type': 'application/json'
			}
		}).then((response) => {
			if (response.ok){
				return response.json();
			}
			else{
				return {
					fail: true
				};
			}
		}).then((data) => {
			if (data.fail){
				dispatch(returnLogin(data, false))
			}
			else{
				dispatch(returnLogin(data, true))
			}
		})
		.catch(() => {dispatch(returnLogin({}, false));});
	};
	/*return dispatch => {
		//return (123);
		//dispatch(requestLogin(email, password));
		return (123);
	}*/
}

export function getItems(){
	return function(dispatch){
		dispatch(requestGetItems());

		return fetch("http://localhost:4000/api/items", {
			method: "GET"
		}).then((response) => {
			if (response.ok){
				return response.json();
			}
			else{
				return {
					fail: true
				};
			}
		}).then((data) => {
			if (data.fail){
				dispatch(returnGetItems(data, false));
			}
			else{
				dispatch(returnGetItems(data, data.success));
			}
		})
		.catch(() => {dispatch(returnGetItems({}, false));});
	}
}


export function buyItem(userID, itemID){
	return function(dispatch){
		dispatch(requestBuy());
		return fetch("http://localhost:4000/api/food", {
			method: "POST",
			body: JSON.stringify({
				sid: userID,
				itemID: itemID
			}),
			headers:{
				'Content-Type': 'application/json'
			}
		}).then((response) => {
			if (response.ok){
				return response.json();
			}
			else{
				return {
					fail: true
				};
			}
		}).then((data) => {
			if (data.fail){
				dispatch(returnBuy(data, false));
			}
			else{
				dispatch(returnBuy(data, data.success));
			}
		})
		.catch(() => {dispatch(returnBuy(null, false));});
	}
}


export function logout(){
	return {
		type: "LOGOUT"
	}
}