import React, { Component } from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import {getItems, selectItem} from './../../actions/action.js';

export class Store extends Component{
  render(){
    const items = this.props.items;
    const isFetching = this.props.isFetching;
    const itemDisplay = items.map((item, idx, arr) => {
      return(<div key = {idx} className = "col-md-4"><DisplayElem item = {item} onBuy = {this.props.selectItem}/>
        </div>);
    });
    if (isFetching){
      return (<div>Fetching Items...</div>);
    }
    if (this.props.selectedItem){
      return <Redirect to = "/buy"/>;
    }
    return (<div>
        {itemDisplay}
      </div>);
  }

  componentDidMount(){
    this.props.getItems();
  }
  
}

const mapStateToProps = (state) => {
  return {
    items: state.shopItems,
    selectedItem: state.selectedItem,
    isFetching: state.isFetching
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getItems: () => {dispatch(getItems())},
    selectItem: (id) => {dispatch(selectItem(id))}
  };
}

export const SmartStore = connect(mapStateToProps, mapDispatchToProps)(Store);

const DisplayElem = ({item, onBuy}) => {
  const image = item.image;
  return (
    <div className = "box box-default" style = {
      {
        minHeight: "200px",
        backgroundImage: "url(" + image + ")",
        backgroundSize: "cover"
      }
    }>
      <div className = "box-header">
        <h4 className = "box-title item-title">{item.name}</h4>
      </div>
      <div className = "box-body item-body">
        <p>{item.description}</p>
        <button style = {{
          position: "absolute",
          bottom: "10px",
          right: "10px"
        }} className = "btn btn-primary pull-right" onClick = {
          () => {
            onBuy(item.id);
          }
        }>Buy</button>
      </div>
    </div>)
}