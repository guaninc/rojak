import React, { Component } from 'react';
import {checkLogin} from './../../actions/action.js';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';

let Login = ({doLogin, warning, success}) => {
  let email, password;

  if (success){
    return <Redirect to = '/store'/>;
  }
  
  return (<div>
      <div className = "row">
        <div className = "col-md-4 col-md-offset-4" style = {{marginTop: "150px"}}>
          <WarningBox warning = {warning}/>
          <div className = "box box-solid box-default box-primary">
            <div className = "box-header with-border">
              <h2 className = "box-title">Login Page</h2>
            </div>
            <div className = "box-body">
              <form onSubmit = { e => {
                e.preventDefault();
                if (email.value == "" || password.value == ""){
                  return
                }
                doLogin(email.value, password.value);
              }}>
                <div className = "form-group">
                  <label>Email Address</label>
                  <input ref = {
                    (node) => {email = node;}
                  } type = "email" className = "form-control" id = "emailInput"/>
                </div>
                <div className = "form-group">
                  <label>Password</label>
                  <input ref = {
                    (node) => {password = node;}
                  } type = "password" className = "form-control" id = "passwordInput"/>
                </div>
                <input type = "submit" className = "btn btn-primary" value = "Log In"/>
              </form>
            </div>
          </div>
        </div>
      </div>
  	</div>);
}

const WarningBox = ({warning}) => {
  console.log(warning);
  if (warning){
    return (<div className = "box box-solid box-danger">
        <div className = "box-header with-border">
          <h3 className = "box-title">Error</h3>
        </div>
        <div className = "box-body">
          {warning}
        </div>
      </div>);
  }
  else{
    return (<div></div>);
  }
}

const mapStateToProps = (state) => {
  let success = false;
  if (state.user.user){
    success = true;
  }
  if (state.hasError){
    return {
      warning: state.hasError,
      success
    };
  }
  else{
    return {
      warning: false,
      success
    };
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      doLogin: (email, password) => {dispatch(checkLogin(email, password));}
    };
}

Login = connect(mapStateToProps, mapDispatchToProps)(Login);

export default Login;