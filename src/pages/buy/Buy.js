import React, { Component } from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import {unselectItem, buyItem, removeSwappedItem} from '../../actions/action.js';

const Buy = ({item, cancelItem, isTransferring, buyItem, swappedItem, returnToStore}) => {
  if (item == null){
    return <Redirect to = "/store" />;
  }
  if (isTransferring){
    return (<div>
        <img src = {item.image} width = "200" height = "200" style = {{
          animationName: "spin",
          animationDuration: "4000ms",
          animationIterationCount: "infinite",
          animationTimingFunction: "linear"
        }
        }/>
      </div>)
  }
  else if (swappedItem){
    return (<div>
        <div className = "box box-solid box-success">
          <div className = "box-header with-border">
            <h3 className = "box-title">Order Success</h3>
          </div>
          <div className = "box-body">
            <div className = "container-fluid">
              <div className = "row">
                <div className = "col-md-4">
                  <ItemDisplay item = {item} />
                </div>

                <div className = "col-md-4" style = {{
                  textAlign: "center"
                }}>
                  <i className = "fa fa-arrow-right fa-5x" style = {{
                    marginTop: "100px"
                  }}></i>
                </div>

                <div className = "col-md-4">
                  <ItemDisplay item = {swappedItem} />
                </div>
                
                
              </div>
            </div>
          </div>
          <div className = "box-footer">
            <button className = "btn btn-primary pull-right" onClick = {() => {returnToStore();}}>Back</button>
          </div>
        </div>
      </div>)
  }
  else{
    return (<div>
    		<div className = "box box-solid box-primary">
          <div className = "box-header with-border">
            <h3 className = "box-title">Confirm Order</h3>
          </div>
          <div className = "box-body">
            <h4>Would you like to confirm your order?</h4>
            <ItemDisplay item = {item} />
          </div>
          <div className = "box-footer">
            <button className = "btn btn-danger pull-right" onClick = {() => {cancelItem();}} style = {{
              marginLeft: "10px"
            }}>Cancel</button>
            <button className = "btn btn-primary pull-right" onClick = {() => {buyItem(item.id);}} style = {{
              marginRight: "10px"
            }}>Buy</button>
          </div>
        </div>
    	</div>);
  }
}

const mapStateToProps = (state) => {
  if (state.shopItems.length > state.selectedItem){
    return {
      item: state.shopItems.find((item) => {return item.id == state.selectedItem}),
      isTransferring: state.isFetching,
      swappedItem: state.swappedItem
    };
  }
  else{
    return {
      item: null
    };
  }
}

function doItemBuy(itemID){
  return (dispatch, getState) => {
    dispatch(buyItem(getState().user.sid, itemID));
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    cancelItem: () => {dispatch(unselectItem());},
    buyItem: (itemID) => {dispatch(doItemBuy(itemID));},
    returnToStore: () => {dispatch(removeSwappedItem())}
  }
}

const ItemDisplay = ({item}) => {
  return (
    <div className = "box box-solid box-default">
      <div className = "box-header with-border">
        <h3 className = "box-title kaushan">{item.name}</h3>
      </div>
      <div className = "box-body">
        <img src = {item.image} width = "200" height = "200" style = {{
          borderRadius: "25px",
          margin: "25px"
        }}/>
        <section>
          <p className = "lead">{item.description}</p>
        </section>
        <table className = "table">
          <tbody>
            <tr><td>Race</td><td style = {{textTransform: "capitalize"}}>{item.race}</td></tr>
            <tr><td>Vegetarian?</td><td><input type = "checkbox" checked = {item.veg} readOnly = "readOnly"/></td></tr>
            <tr><td>Halal?</td><td><input type = "checkbox" checked = {item.halal} readOnly = "readOnly"/></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export const SmartBuy = connect(mapStateToProps, mapDispatchToProps)(Buy);