import React, { Component } from 'react';

export const Home = () => {
  return (<div>
  		<section style = {{
        marginTop: "25px"
      }}>
  			<h2 className = "page-header">The Story of Rojak</h2>
  			<p className = "lead">
  				In Japan, there's a restaurant where patrons buy food for those that come after them
          and consume the food bought by those before them. We found this concept unique and
          interesting and plan to take it to the next level. Introducing <span className = "kaushan">Rojak
          </span>, the app that lets you order food and swap it with someone else's order.
  			</p>
  		</section>
  		<section style = {{
        marginTop: "75px"
      }}>
        <h2 className = "page-header">Swap Food Swap Culture</h2>
        <div style = {{
          textAlign: "center"
        }}>
          <img className = "img-disp" src = "http://farm6.static.flickr.com/5225/5615985674_08f5369690_z.jpg" width = "200" height = "200"/>
          <i className = "fa fa-arrows-h fa-5x"></i>
          <img className = "img-disp" src = "http://thewoksoflife.com/wp-content/uploads/2016/06/grilled-tandoori-chicken-2.jpg" width = "200" height = "200" />
        </div>
  			<p className = "lead">
	  			The first step in understanding culture is through food. When you visit a foreign country,
          the first thing you experience is food. Singaporeans are bound together by our love of food,
          whether it's Chilli Crab or Nasi Lemak and food is a good way to gain an understanding of other
          cultures. In our modern society, we don't often consume food from other cultures. Rojak helps
          provide a first step to understanding other races or cultures by experiencing their food.
  			</p>
  		</section>
  		<section style = {{
        marginTop: "75px"
      }}>
        <h2 className = "page-header">Our Secret Sauce</h2>
  			<p className = "lead">
          Behind <span className = "kaushan">Rojak</span> is an algorithm that decides how to
          swap the food. To make this algorithm, we take into account the dietary requirements
          of our customers, halal food, vegetarian food or no nuts and no beans. At the same time,
          we try to swap for food of the same price and we swap for food that is far from the user's
          race as much as possible.
  			</p>
  		</section>
  		<section style = {{
        marginTop: "75px"
      }}>
        <h2 className = "page-header">More Than Just Food</h2>
        <p className = "lead">
          <span className = "kaushan">Rojak</span> is not limited to just food. <span className = "kaushan">Rojak</span>
           &nbsp;is an experiment in the sharing of cultural values. If the idea works out, we can extend <span className = "kaushan">Rojak</span>
           &nbsp;to homestays and other cultural items such as games or paintings. 
        </p>
      </section>
  	</div>);
}